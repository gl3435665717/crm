package com.huike.web.controller.review;


import com.huike.common.core.controller.BaseController;
import com.huike.common.core.domain.AjaxResult;
import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.service.ReviewService;
import com.huike.review.vo.MybatisReviewVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 该Controller主要是为了复习三层架构以及Mybatis使用的，该部分接口已经放开权限，可以直接访问
 * 同学们在此处编写接口通过浏览器访问看是否能完成最简单的增删改查
 */
@RestController
@RequestMapping("/review")
public class MybatisReviewController extends BaseController {

    @Autowired
    private ReviewService reviewService;

    /**=========================================================新增数据============================================*/
    @GetMapping("/saveData/{name}/{age}/{sex}")
    public AjaxResult addData(@PathVariable String name,@PathVariable Integer age,@PathVariable String sex){
        reviewService.addData(name,age,sex);
        return AjaxResult.success("成功插入:1条数据");
    }
    @PostMapping("/saveData")
    public AjaxResult addData2(@RequestBody Review review){
        reviewService.addData2(review);
        return AjaxResult.success("成功插入:1条数据");
    }
    /**=========================================================删除数据=============================================*/

    @DeleteMapping("/remove/{id}")
    public AjaxResult deleteById(@PathVariable Long id){
        reviewService.deleteById(id);
        return AjaxResult.success("成功删除:1条数据");

    }
    /**=========================================================修改数据=============================================*/

    @PostMapping("/update")
    public AjaxResult updateData(@RequestBody Review review){
        reviewService.updateData(review);
        return AjaxResult.success("修改成功");
    }
    /**=========================================================查询数据=============================================*/

    @GetMapping("/getById")
    public AjaxResult getById(Long id){
        Review review = reviewService.getById(id);
        if(review != null){
            return AjaxResult.success("操作成功",review);
        }
        return AjaxResult.error("操作失败");
    }

    @GetMapping("/getDataByPage")
    public TableDataInfo getDataByPage(){
        startPage();
        List<Review> reviewList = reviewService.getDataByPage();
        return getDataTablePage(reviewList);
    }
}