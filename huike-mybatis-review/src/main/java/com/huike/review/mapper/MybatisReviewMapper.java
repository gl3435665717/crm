package com.huike.review.mapper;

import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.vo.MybatisReviewVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Mybatis复习的Mapper层
 */
public interface MybatisReviewMapper {



    /**======================================================新增======================================================**/
    void addData(@Param("name") String name, @Param("age") Integer age, @Param("sex") String sex);

    void addData2(Review review);


    /**======================================================删除======================================================**/

    void deleteById(Long id);
    /**======================================================修改======================================================**/
    void updateData(Review review);



    /**======================================================简单查询===================================================**/

    Review getById(Long id);

    List<Review> getDataByPage();

}
