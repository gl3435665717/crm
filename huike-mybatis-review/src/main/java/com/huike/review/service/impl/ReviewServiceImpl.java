package com.huike.review.service.impl;

import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;
import com.huike.review.service.ReviewService;
import com.huike.review.mapper.MybatisReviewMapper;
import com.huike.review.vo.MybatisReviewVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * mybatis复习使用的业务层
 * 注意该业务层和我们系统的业务无关，主要是让同学们快速熟悉系统的
 */
@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private MybatisReviewMapper reviewMapper;



    /**=========================================================保存数据的方法=============================================*/
    @Override
    public void addData(String name, Integer age, String sex) {
        reviewMapper.addData(name,age,sex);
    }

    @Override
    public void addData2(Review review) {
        reviewMapper.addData2(review);
    }


    /**=========================================================删除数据=============================================*/


    @Override
    public void deleteById(Long id) {
        reviewMapper.deleteById(id);
    }



    /**=========================================================修改数据=============================================*/

    @Override
    public void updateData(Review review) {
        reviewMapper.updateData(review);
    }

    /**=========================================================查询数据的方法=============================================*/

    @Override
    public Review getById(Long id) {
        Review review = reviewMapper.getById(id);
        return review;
    }

    @Override
    public List<Review> getDataByPage() {
        List<Review> reviewList = reviewMapper.getDataByPage();
        return reviewList;
    }
}
