package com.huike.review.service;

import com.huike.common.core.page.TableDataInfo;
import com.huike.review.pojo.Review;

import java.util.List;

/**
 * mybatis复习接口层
 */
public interface ReviewService {


    //新增方法get请求
    void addData(String name, Integer age, String sex);

    //新增方法post请求
    void addData2(Review review);

    //修改方法post请求
    void updateData(Review review);

    //根据ID删除数据delete请求
    void deleteById(Long id);

    //根据id查询数据get请求
    Review getById(Long id);

    //分页
    List<Review> getDataByPage();

}
